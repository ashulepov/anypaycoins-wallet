#!/usr/bin/env bash

# Backup Directory
BD=$1
DST=$2

echo "Backup path:" $1

# Restore env
cp $BD/.env $DST/.env
if [ ! -f .env ]; then
    echo "Restore .env error!"
    exit 1
fi
echo "Env:         " $DST/.env

# Export .env vars
export $(egrep -v '^#' .env | xargs)

# Restore configs
cp $BD/conf/api.conf $DST/conf/api.conf
cp $BD/conf/geth-api.conf $DST/conf/geth-api.conf
if [ ! -f $DST/conf/api.conf ]; then
    echo "Restore configs (api.conf) error!"
    exit 1
fi
if [ ! -f $DST/conf/geth-api.conf ]; then
    echo "Restore configs (geth-api.conf) error!"
    exit 1
fi
echo "Сonfigs:     " $DST/conf

# Restore db
docker-compose up -d db
echo "Sleep 15 secs while db starting"
sleep 15
cat $BD/postgresql/${POSTGRES_DB:-wallet}.sql | docker-compose exec -T db psql -U ${POSTGRES_USER:-postgres}
docker-compose stop db
if [ ! -d $DST/data/postgresql ]; then
    echo "Restore database error!"
    exit 1
fi
echo "Database:    " $DST/data/postgresql

# Restore Ethereum
mkdir -p $DST/data/ethereum/.ethereum
cp -R $BD/ethereum/* $DST/data/ethereum/.ethereum
if [ ! -d $DST/data/ethereum/.ethereum ]; then
    echo "Restore ethereum error!"
    exit 1
fi
echo "Ethereum:    " $BD/data/ethereum/.ethereum

# Restore Bitcoin
docker-compose up -d bitcoin
echo "Sleep 15 secs while bitcoin starting"
sleep 15
docker cp $BD/bitcoin/wallet.dat apc-bitcoin:/tmp
docker-compose exec -T bitcoin chmod 644 /tmp/wallet.dat
docker-compose exec -T bitcoin bitcoin-cli -rpcuser=${BITCOIN_RPC_USER:-rpcuser} -rpcpassword=${BITCOIN_RPC_PASSWORD:-rpcpassword} -rpcport=${BITCOIN_RPC_PORT:-8333/tcp} importwallet /tmp/wallet.dat
docker-compose exec -T bitcoin rm /tmp/wallet.dat
docker-compose stop bitcoin
if [ ! -d $DST/data/bitcoin/.bitcoin ]; then
    echo "Restore bitcoin error!"
    exit 1
fi
echo "Bitcoin:     " $DST/data/bitcoin/.bitcoin

# Restore Bitcoin Cash
docker-compose up -d bitcoin-cash
echo "Sleep 15 secs while bitcoin cash starting"
sleep 15
docker cp $BD/bitcoin-cash/wallet.dat apc-bitcoin-cash:/tmp
docker-compose exec -T bitcoin-cash chmod 644 /tmp/wallet.dat
docker-compose exec -T bitcoin-cash bitcoin-cli -rpcuser=${BITCOIN_CASH_RPC_USER:-rpcuser} -rpcpassword=${BITCOIN_CASH_RPC_PASSWORD:-rpcpassword} -rpcport=${BITCOIN_CASH_RPC_PORT:-8332/tcp} importwallet /tmp/wallet.dat
docker-compose exec -T bitcoin-cash rm /tmp/wallet.dat
docker-compose stop bitcoin-cash
if [ ! -d $DST/data/bitcoin-cash/.bitcoin ]; then
    echo "Restore bitcoin cash error!"
    exit 1
fi
echo "Bitcoin Cash:" $BD/bitcoin-cash

# Restore Litecoin
docker-compose up -d litecoin
echo "Sleep 15 secs while litecoin starting"
sleep 15
docker cp $BD/litecoin/wallet.dat apc-litecoin:/tmp
docker-compose exec -T litecoin chmod 644 /tmp/wallet.dat
docker-compose exec -T litecoin litecoin-cli -rpcuser=${LITECOIN_RPC_USER:-rpcuser} -rpcpassword=${LITECOIN_RPC_PASSWORD:-rpcpassword} -rpcport=${LITECOIN_RPC_PORT:-8332/tcp} importwallet /tmp/wallet.dat
docker-compose exec -T litecoin rm /tmp/wallet.dat
docker-compose stop litecoin
if [ ! -d $DST/data/litecoin/.litecoin ]; then
    echo "Restore litecoin error!"
    exit 1
fi
echo "Litecoin:" $BD/litecoin

exit 0