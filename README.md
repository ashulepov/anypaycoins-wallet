# AnyPayCoins offline wallets

    Api endpoint: http://127.0.0.1:8099
    Authorization: Bearer token

## Installation

### Git LFS 

    https://github.com/git-lfs/git-lfs/wiki/Installation

### Clone AnyPayCoins Wallet from repository

    git clone https://github.com/icobox/anypaycoins-wallet.git
    
### Retrieve credentials from AnyPayCoins support

    Provide to Anypaycoins support:
    
        Wallet API Url 

    Retrieve credentials from AnyPayCoins support:
    
        Wallet API Access Token (api.conf: wallet::accesstoken)
        Wallet API Public Key (api.conf: wallet::publickey) 

### Set environment variables (default values below):
   
#### PostgreSQL
    
    POSTGRES_DB=wallet
    POSTGRES_USER=postgres (do not use default value)
    POSTGRES_PASSWORD=12345 (do not use default value)

#### Migrations
    
    DB_DRIVER=postgres
    DB_OPEN=host=db user=postgres password=12345 dbname=wallet sslmode=disable (do not use default value)
     
#### Ethereum
    
    ETHEREUM_RPC_ADDR=0.0.0.0
    ETHEREUM_RPC_PORT=8545
    ETHEREUM_TESTNET=
    
#### Bitcoin
    
    BITCOIN_RPC_ALLOWIP=0.0.0.0/0
    BITCOIN_RPC_USER=rpcuser (do not use default value)
    BITCOIN_RPC_PASSWORD=rpcpassword (do not use default value)
    BITCOIN_RPC_PORT=8333/tcp
    BITCOIN_TESTNET=0
    
#### Bitcoin Cash
    
    BITCOIN_CASH_RPC_ALLOWIP=0.0.0.0/0
    BITCOIN_CASH_RPC_USER=rpcuser (do not use default value)
    BITCOIN_CASH_RPC_PASSWORD=rpcpassword (do not use default value)
    BITCOIN_CASH_RPC_PORT=8332/tcp
    BITCOIN_CASH_TESTNET=0
    
### Encrypt wallets

    To encrypt Bitcoin wallet run command inside node container:
   
    $ bitcoin-cli -rpcuser=[rpcuser] -rpcpassword=[rpcpassword] -rpcport=8333 encryptwallet [walletpassphrase]
   
    To encrypt Bitcoin Cash wallet run command inside node container:
   
    $ bitcoin-cli -rpcuser=[rpcuser] -rpcpassword=[rpcpassword] -rpcport=8332 encryptwallet [walletpassphrase]
        
### Config files

    Copy default config files and change default values

    $ cp ./conf/api.conf.example ./conf/api.conf
    $ cp ./conf/geth-api.conf.example ./conf/geth-api.conf
    
#### Api.conf
    
    wallet::addresspool = 100 - address pool size for each currency
    wallet::publickey = "" - Wallet Api Public Key retrieved from AnyPayCoins
    wallet::accesstoken = "" - Wallet Api Access Token retrieved from Anypaycoins
    
    db::open = "host=db user=postgres password=12345 dbname=wallet sslmode=disable" - Db connection string
    
    bitcoin::host = "bitcoin:8333" - Bitocin Node address
    bitcoin::user = "rpcuser" - Bitcoin Node Api username
    bitcoin::pass = "rpcpassword" - Bitcoin Node Api password
    bitcoin::passphrase = "" - Bitcoin Node Wallet Passphrase
    
    bitcoin-cash::host = "bitcoin:8333" - Bitocin Node address
    bitcoin-cash::user = "rpcuser" - Bitcoin Node Api username
    bitcoin-cash::pass = "rpcpassword" - Bitcoin Node Api password
    bitcoin-cash::passphrase = "" - Bitcoin Node Wallet Passphrase
    
    ethereum::host = "http://ethereum:8545" - Ethereum Node address
    ethereum::api = "http://geth-api:8081" - Geth Api address
    ethereum::accesstoken = "secret" - Geth Api Access Token (do not use default value)

#### Geth-api.conf
    
    wallet::accesstoken = "secret" -  Geth Api Access Token (do not use default value)
    wallet::keystore = "/home/user/.ethereum/keystore" - Path to keystore files where ethereum client stores accounts
    
## Run

    $ docker-compose up
    
## Persistent data
    
    All data (db, keystore, wallets) stores in "./data" directory

## Backup/restore data

    Backup:
    ./backup.sh
    
    Restore:
    ./restore.sh [/backup/path] [/restore/path]
    ./restore.sh ./backup/2018-06-05-18-13-38/ .